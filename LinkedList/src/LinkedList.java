
public class LinkedList {

	private Element first;
	private Element last;

	public LinkedList() {
		first = null;
		last = null;
	}

	public void insertFirst(Object obj) {
		Element e = new Element(obj);
		if (first == null) {
			last = e;
		} else {
			e.setNext(first);
		}
		first = e;
	}

	public void insertLast(Object obj) {
		Element e = new Element(obj);
		if (last == null) {
			first = e;
		} else {
			last.setNext(e);
		}
		last = e;
	}

	public void print() {
		Element e = first;
		while (e != null) {
			System.out.print(e.getObject() + " ");
			e = e.getNext();
		}
		System.out.println();
	}

	public boolean isEmpty() {
		if (first == null) {
			return true;
		} else {
			return false;
		}
	}

	/*
	 * public void remove(Object obj) throws ObjectNotFoundException,
	 * EmptyListException { if (isEmpty()) { EmptyListException e = new
	 * EmptyListException("The list is empty"); throw e; } Element ant = first;
	 * Element e = first; boolean delete = false; while (e != null) { if
	 * (e.getObject().equals(obj)) { ant.setNext(e.getNext()); if (e.getNext() ==
	 * null) { last = ant; } delete = true; } else { ant = e; e = e.getNext(); } }
	 * if (!delete) { ObjectNotFoundException ob = new
	 * ObjectNotFoundException("Object not found"); throw ob; }
	 * 
	 * }
	 */

	public void remove(Object object) throws ObjectNotFoundException, EmptyListException {

		if (isEmpty()) {
			throw new EmptyListException();
		}

		Element current, previous;

		current = first;
		previous = first;

		while (current != null && !current.getObject().equals(object)) {
			previous = current;
			current = current.getNext();
		}
		if (current == null) {
			throw new ObjectNotFoundException("Element " + object + " not found in the list");
		}

		if (first == last) {
			first = null;
			last = null;
		} else {
			if (current == first) {
				first = current.getNext();
			} else {
				previous.setNext(current.getNext());
			}
			if (current == last) {
				last = previous;
			}
		}

	}

	public Object getFirstObject() throws EmptyListException {
		if (isEmpty()) {
			EmptyListException e = new EmptyListException("The list is empty");
			throw e;
		}
		return first.getObject();
	}

	public Object getLastObject() throws EmptyListException {
		if (isEmpty()) {
			EmptyListException e = new EmptyListException("The list is empty");
			throw e;
		}
		return last.getObject();
	}

	public int getNumElements() {
		Element e = first;
		int count = 0;
		while (e != null) {
			count++;
			e = e.getNext();
		}
		return count;
	}

	public Object getObjectAtPosition(int i) throws EmptyListException, InvalidIndexException {
		if (isEmpty()) {
			EmptyListException e = new EmptyListException("The list is empty");
			throw e;
		}
		Element e = first;
		int count = 1;
		while (e != null) {
			if (count == i) {
				return e.getObject();
			} else {
				e = e.getNext();
				count++;
			}
		}
		if (i >= count) {
			InvalidIndexException in = new InvalidIndexException("The position is bigger than the index");
			throw in;
		}
		throw new Error("This should never happen, if happends, we are fucked");
	}
}
