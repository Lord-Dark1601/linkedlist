
public class ObjectNotFoundException extends LinkedListException {

	public ObjectNotFoundException() {
		super("Object not found Exception");
	}

	public ObjectNotFoundException(String msg) {
		super(msg);
	}
}
