
public class InvalidIndexException extends LinkedListException {

	public InvalidIndexException() {
		super("Invalid index Exception");
	}

	public InvalidIndexException(String msg) {
		super(msg);
	}
}
