
public class Stack {

	private LinkedList l;

	public Stack() {
		l = new LinkedList();
	}

	public void push(Object obj) {
		l.insertFirst(obj);
	}

	public Object pop() throws EmptyListException, ObjectNotFoundException {
		Object ob = l.getFirstObject();
		l.remove(l.getFirstObject());
		return ob;
	}

	public boolean isEmpty() {
		if (l.isEmpty()) {
			return true;
		} else {
			return false;
		}
	}

	public void empty() throws EmptyListException, ObjectNotFoundException {
		int numObj = l.getNumElements();
		for (int i = 0; i < numObj; i++) {
			l.remove(l.getFirstObject());
		}
	}

	public void print() {
		l.print();
	}
}
