
public class EmptyListException extends ObjectNotFoundException {

	public EmptyListException() {
		super("Empty list Exception");
	}

	public EmptyListException(String msg) {
		super(msg);
	}
}
